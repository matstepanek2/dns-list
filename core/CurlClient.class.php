<?php

/**
 * Class CurlClient
 *
 * Client for working with requests
 */
class CurlClient
{
  const REQUEST_GET = 'GET';
  const REQUEST_POST = 'POST';
  const REQUEST_DELETE = 'DELETE';
  const RECORD_TYPES = array('A', 'AAAA', 'MX', 'ANAME', 'CNAME', 'NS', 'TXT', 'SRV');

  const PATH_LIST_ZONES = 'zone';
  const BASE_PATH = '/v1/user/self/';

  /**
   * @var array|bool
   */
  protected $config;

  /**
   * Assuming we are creating client only for one domain
   *
   * @var string
   */
  protected $domain;

  /**
   * CurlClient constructor.
   */
  public function __construct() {
    $this->config = parse_ini_file('config.ini', true);
    $this->domain = $this->config['domain'];
  }

  /**
   * Method for Curl requests GET, POST, DELETE
   *
   * @param $path_request
   * @param string $type
   * @param null $data
   * @return mixed
   */
  public function sendRequest($path_request, $type = 'GET', $data = null) {
    $time = time();
    $method = $type;
    $path = self::BASE_PATH . $path_request;
    $canonicalRequest = sprintf('%s %s %s', $method, $path, $time);
    $signature = hash_hmac('sha1', $canonicalRequest, $this->config['secret']);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, sprintf('%s:%s', $this->config['api'], $path));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch, CURLOPT_USERPWD, $this->config['api_key'].':'.$signature);
    curl_setopt($ch, CURLOPT_HTTPHEADER, [
      'Date: ' . gmdate('Ymd\THis\Z', $time),
      'Content-Type: application/json',
      'Accept: application/json'
    ]);

    if ($method === self::REQUEST_DELETE) {
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
    }

    if (!empty($data)) {
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    }

    $response = curl_exec($ch);
    if (curl_errno($ch)) {
      $response = 'Curl error: ' . curl_error($ch);
    }
    curl_close($ch);

    return $response;
  }

  /**
   *
   * @return mixed
   */
  public function getDomain() {
    return $this->domain;
  }

  /**
   * List zones
   *
   * @return mixed
   */
  public function listZones() {
    return $this->sendRequest(self::PATH_LIST_ZONES, self::REQUEST_GET);
  }

  /**
   * List all records
   *
   * @return mixed
   */
  public function listRecords() {
    return $this->sendRequest('zone/' . $this->domain . '/record', self::REQUEST_GET);
  }

  /**
   * Create new record
   *
   */
  public function createRecord() {

    if (isset($_POST['submit'])) {
      $arr = array(
        'type' => $_POST['type'],
        'name' => $_POST['name'],
        'content' => $_POST['content'],
        'ttl' => $_POST['ttl'],
        'note' => $_POST['note'],
      );
      if (($_POST['type'] === 'MX') || ($_POST['type'] === 'SRV') ) {
        $arr['prio'] = $_POST['prio'];
      }
      if ($_POST['type'] === 'SRV') {
        $arr['port'] = $_POST['port'];
        $arr['weight'] = $_POST['weight'];
      }

      $json_data = json_encode($arr);
      $response_json = $this->sendRequest('zone/' . $this->domain . '/record', self::REQUEST_POST, $json_data);
      $response = json_decode($response_json);

      if (!empty($response)) {
        if (isset ($response->status)) {
          if (($response->status === 'error') && !empty($response->errors)) {
            foreach ($response->errors as $key => $value) {
              $_SESSION['error'][] = "$key: $value[0]";
              $_SESSION['error_form'] = true;
            }
          } else if ($response->status === 'success') {
            $_SESSION['success'] = 'Data successfully added';
          }
        } else if (isset($response->message)) {
          $_SESSION['error'][] = 'Code ' . $response->code . ': ' . $response->message;
          $_SESSION['error_form'] = true;
        }
      }

      // data for form if error occurred
      if (isset($_SESSION['error'])) {
        $_SESSION['form_data'] = $arr;
      }

      header('Location: /');
    }
  }

  /**
   * Method to delete record
   *
   * @param $id
   */
  public function deleteRecord($id) {

    if ($id !== null) {
      $response_json = $this->sendRequest('zone/' . $this->domain . '/record/' . $id, self::REQUEST_DELETE);
      $response = json_decode($response_json);

      if (!empty($response)) {
        if (isset ($response->status)) {
          if ($response->status === 'success') {
            $_SESSION['success'] = 'Record successfully deleted';
          }
        } else if (isset($response->message)) {
          $_SESSION['error'][] = 'Code ' . $response->code . ': ' . $response->message;
        }
      }
    } else {
      $_SESSION['error'][] = 'Error: ID not passed.';
    }

    header('Location: /');
  }
}
