<?php

/**
 * Class RouterClient
 * Simple router
 */
class Router {

  /**
   * @var CurlClient instance
   */
  protected $cClient;

  /**
   * Router constructor.
   * @param $cClient
   */
  public function __construct($cClient) {
    $this->cClient = $cClient;
  }

  /**
   * Set all routes for the website, including templates
   */
  public function setRoutes() {
    $request = $_SERVER['REQUEST_URI'];

    switch ($request) {

      case '/' :
        session_start();
        $records = json_decode($this->cClient->listRecords());
        usort($records->items, function($a, $b) { return strcmp($a->type, $b->type); });
        $domain = $this->cClient->getDomain();
        $record_types = CurlClient::RECORD_TYPES;
        require __DIR__ . '/../views/index.php';
        session_destroy();
        break;

      case '/add-record' :
        session_start();
        $_SESSION = array();
        $this->cClient->createRecord();
        break;

      case stristr($request, '/delete-record') :
        session_start();
        $_SESSION = array();
        if (isset($_GET['id'])) {
          $this->cClient->deleteRecord($_GET['id']);
        } else {
          $_SESSION['error'][] = 'Error: ID not passed.';
        }
        break;

      default:
        http_response_code(404);
        require __DIR__ . '/../views/404.php';
        break;
    }
  }
}