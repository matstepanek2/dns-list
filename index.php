<?php

include_once 'core/CurlClient.class.php';
include_once 'core/Router.class.php';

$cClient = new CurlClient();
$router = new Router($cClient);
$router->setRoutes();
