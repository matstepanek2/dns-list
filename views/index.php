<!DOCTYPE html>
<html lang="sk">
<head>
  <title>DNS Management</title>

  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600;700&family=Roboto:wght@300;400;500;700&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" href="/src/scss/style.css">
  <script src="/src/js/main.js" defer></script>
</head>
<body>

<header>
  <nav class="navbar navbar-dark bg-dark">
    <div class="container">
      <a class="navbar-brand" href="/"><h1>DNS Management</h1></a>
      <span class="navbar-text">
        <?php print $domain; ?>
      </span>
    </div>
  </nav>
</header>

<main>
  <section class="container mt-5">
    <div class="add-record-section">

      <button id="add-record-button" type="button" class="btn btn-outline-primary mb-3">
        Add new record
        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-plus-circle" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
          <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"></path>
          <path fill-rule="evenodd" d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z"></path>
        </svg>
      </button>

      <?php if (isset($_SESSION['error'])): ?>
        <div class="alert alert-danger" role="alert">
          <?php foreach($_SESSION['error'] as $error): ?>
            <?php print $error; ?><br>
          <?php endforeach; ?>
        </div>
      <?php endif; ?>

      <?php if (isset($_SESSION['success'])): ?>
        <div class="alert alert-success" role="alert">
          <?php print $_SESSION['success']; ?>
        </div>
      <?php endif; ?>

      <form id="add-record-form" action="/add-record" method="post" <?php print isset($_SESSION['error_form']) ? 'class="active"' : ''; ?>>
        <div class="input-group mb-3">

          <div class="input-group-prepend">
            <label class="input-group-text" for="type">Typ záznamu</label>
          </div>

          <select name="type" class="custom-select" id="type" required>
            <option>Choose</option>
            <?php foreach ($record_types as $type): ?>
              <option value="<?php print $type; ?>" <?php print (isset($_SESSION['form_data']['type']) && ($type === $_SESSION['form_data']['type'])) ? 'selected' : ''; ?>><?php print $type; ?></option>
            <?php endforeach; ?>
          </select>
        </div>

        <label for="name">Name</label>
        <small id="name-help-text" class="form-text text-muted">subdomain name or @ if you don't want subdomain</small>
        <div class="input-group mb-3">
          <input name="name" type="text" class="form-control" placeholder="www" required
            <?php print isset($_SESSION['form_data']['name']) ? 'value="' . $_SESSION['form_data']['name'] . '"' : ''; ?>>
          <div class="input-group-append">
            <span class="input-group-text" id="name"><?php print $domain; ?></span>
          </div>
        </div>

        <div class="form-group">
          <label for="content">Content</label>
          <small id="content-help-text" class="form-text text-muted">IPv4 address in dotted decimal format, i.e. 1.2.3.4</small>
          <input name="content" type="text" class="form-control" id="content" placeholder="127.0.0.1" required
            <?php print isset($_SESSION['form_data']['content']) ? 'value="' . $_SESSION['form_data']['content'] . '"' : ''; ?>>
        </div>

        <div id="priority-input" class="form-group"
          <?php print (isset($_SESSION['form_data']['type']) && (($_SESSION['form_data']['type'] === 'MX') || ($_SESSION['form_data']['type'] === 'SRV'))) ? 'style="display: block;' : '' ?>>
          <label for="priority">Priority</label>
          <small class="form-text text-muted">record priority</small>
          <input name="prio" type="number" class="form-control" id="priority"
            <?php print isset($_SESSION['form_data']['priority']) ? 'value="' . $_SESSION['form_data']['priority'] . '"' : ''; ?>>
        </div>

        <div id="port-input" class="form-group" <?php print (isset($_SESSION['form_data']['type']) && $_SESSION['form_data']['type'] === 'SRV') ? 'style="display: block;' : '' ?>>
          <label for="port">Port</label>
          <small class="form-text text-muted">the TCP or UDP port on which the service is to be found</small>
          <input name="port" type="number" class="form-control" id="port"
            <?php print isset($_SESSION['form_data']['port']) ? 'value="' . $_SESSION['form_data']['port'] . '"' : ''; ?>>
        </div>

        <div id="weight-input" class="form-group" <?php print (isset($_SESSION['form_data']['type']) && $_SESSION['form_data']['type'] === 'SRV') ? 'style="display: block;' : '' ?>>
          <label for="weight">Weight</label>
          <small class="form-text text-muted">a relative weight for records with the same priority</small>
          <input name="weight" type="number" class="form-control" id="weight"
            <?php print isset($_SESSION['form_data']['weight']) ? 'value="' . $_SESSION['form_data']['weight'] . '"' : ''; ?>>
        </div>

        <label for="ttl">TTL</label>
        <small class="form-text text-muted">time to live, default 600</small>
        <div class="input-group mb-3">
          <input name="ttl" type="number" class="form-control" placeholder="600" required
            <?php print isset($_SESSION['form_data']['ttl']) ? 'value="' . $_SESSION['form_data']['ttl'] . '"' : ''; ?>>
          <div class="input-group-append">
            <span class="input-group-text" id="ttl">seconds</span>
          </div>
        </div>

        <div class="form-group">
          <label for="note">Note</label>
          <input name="note" type="text" class="form-control" id="note" placeholder="Note"
            <?php print isset($_SESSION['form_data']['note']) ? 'value="' . $_SESSION['form_data']['note'] . '"' : '' ?>>
        </div>

        <button name="submit" type="submit" class="btn btn-primary">Add record</button>
      </form>
    </div>

    <div class="table-responsive">
      <table class="table table-hover mt-3">
        <thead class="thead-dark">
        <tr>
          <th scope="col">Type</th>
          <th scope="col">Name (For address)</th>
          <th scope="col">Content</th>
          <th scope="col">TTL</th>
          <th scope="col">Note</th>
          <th scope="col">Action</th>
        </tr>
        </thead>

        <tbody>
        <?php foreach ($records->items as $record): ?>
          <tr>
            <th scope="row"><?php print $record->type; ?></th>
            <td><?php print ($record->name !== '@') ? $record->name . '.' . $domain : $domain ?></td>
            <td><?php print $record->content; ?>
            <?php if (($record->type === 'MX') || ($record->type === 'SRV')): ?>
              <br>
              <small class="text-muted">Priority: <?php print $record->prio; ?></small>
              <?php if ($record->type === 'SRV'): ?>
                <br>
                <small class="text-muted">Port: <?php print $record->port; ?></small>
                <br>
                <small class="text-muted">Weight: <?php print $record->weight; ?></small>
              <?php endif; ?>
            <?php endif; ?>
            </td>
            <td><?php print $record->ttl; ?></td>
            <td><?php print $record->note; ?></td>
            <td><a href="/delete-record?id=<?php print $record->id; ?>" type="button" class="btn btn-outline-danger">
                <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-trash-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                  <path fill-rule="evenodd" d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5a.5.5 0 0 0-1 0v7a.5.5 0 0 0 1 0v-7z"></path>
                </svg>
              </a></td>
          </tr>
        <?php endforeach; ?>
        </tbody>

      </table>
    </div>
  </section>

</main>

<footer>
  <hr>
  <div class="container">
    <p class="text-muted text-right">Author: Martin Stepanek</p>
  </div>
</footer>

</body>
