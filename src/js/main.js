/**
 * Javascript functionality
 */

// Static text for messages, could be possibly retrieved with some REST API
var messages = {
  name: {
    default: 'subdomain name or @ if you don\'t want subdomain',
    ANAME: 'value: @ or empty string',
    CNAME: 'subdomain name',
  },
  content: {
    A: 'IPv4 address in dotted decimal format, i.e. 1.2.3.4',
    AAAA: 'IPv6 address ex. 2001:db8::3',
    MX: 'domain name of mail servers, i.e. mail1.scaledo.com',
    ANAME: 'the canonical hostname something.scaledo.com',
    CNAME: 'the canonical hostname something.scaledo.com',
    NS: 'the canonical hostname of the DNS server, i.e. ns1.scaledo.com',
    TXT: 'text used for DKIM or other purposes',
    SRV: 'the canonical hostname of the machine providing the service'
  }
};

var type = document.getElementById('type');
var optionSelected = type.options[type.selectedIndex].value;
setHelpMessages(optionSelected);

document.getElementById('add-record-button').onclick = function() {
  var form = document.getElementById('add-record-form');

  form.classList.toggle('active');
};

type.onchange = function() {
  var optionSelected = this.options[this.selectedIndex].value;

  setHelpMessages(optionSelected);

  document.getElementById('priority-input').style.display = 'none';
  document.getElementById('port-input').style.display = 'none';
  document.getElementById('weight-input').style.display = 'none';

  if (optionSelected === 'MX') {
    document.getElementById('priority-input').style.display = 'block';
  } else if (optionSelected === 'SRV') {
    document.getElementById('priority-input').style.display = 'block';
    document.getElementById('port-input').style.display = 'block';
    document.getElementById('weight-input').style.display = 'block';
  }
};

/**
 * Setting help messages dynamically
 * @param type
 */
function setHelpMessages(type) {
  var name = document.getElementById('name-help-text');
  var content = document.getElementById('content-help-text');
  if (type === 'A') {
    name.textContent = messages.name.default;
    content.textContent = messages.content.A;
  }
  if (type === 'AAAA') {
    name.textContent = messages.name.default;
    content.textContent = messages.content.AAAA;
  }
  if (type === 'MX') {
    name.textContent = messages.name.default;
    content.textContent = messages.content.MX;
  }
  if (type === 'ANAME') {
    name.textContent = messages.name.ANAME;
    content.textContent = messages.content.ANAME;
  }
  if (type === 'CNAME') {
    name.textContent = messages.name.CNAME;
    content.textContent = messages.content.CNAME;
  }
  if (type === 'NS') {
    name.textContent = messages.name.default;
    content.textContent = messages.content.NS;
  }
  if (type === 'TXT') {
    name.textContent = messages.name.default;
    content.textContent = messages.content.TXT;
  }
  if (type === 'SRV') {
    name.textContent = messages.name.default;
    content.textContent = messages.content.SRV;
  }
}
